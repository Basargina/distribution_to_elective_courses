import courses.helpers as helpers
from courses.student import Student
import random
import math

def clustering(arr, helper, centers):
    matrix = generateMatrix(len(arr), len(centers))
    prev = 0
    curr = 1
    a = 0
    while(a < helper.max_cycles() and abs(prev - curr) > helper.eps()) :
        a += 1
        prev = curr
        centers = calculateCenters(matrix, helper, arr)
        new_matrix = []
        for i in range(len(arr)):
            item = []
            for j in range(len(centers)):
                dist = evklidDistance(arr[i], centers[j])
                u = prepareU(dist, helper.fuzz())
                item.append(u)
            new_matrix.append(normalize(item))
        matrix = new_matrix
        curr = calculateFunction(arr, centers, matrix)

    return matrix

def generateMatrix(x, y):
    matrix = []
    for i in range(x):
        item = []
        for j in range(y):
            item.append(random.random())
        matrix.append(normalize(item))
    return matrix

def normalize(item):
    sum_it  = 0
    for i in item:
        sum_it += i

    norm_item = []
    for i in item:
        norm_item.append(i/sum_it)

    return norm_item

def calculateCenters(matrix, helper, points):
    cent = []

    for i in range(helper.cluster_num()):
        tempA = Student('',0,0,0)
        tempB = Student('',0,0,0)
        m = helper.fuzz()
        j = 0
        for item in matrix:
            tempA.score += pow(item[i], m)
            tempB.score += pow(item[i], m) * points[j].score

            tempA.level_eng += pow(item[i], m)
            tempB.level_eng += pow(item[i], m) * points[j].level_eng

            # tempA.language += pow(item[i], m)
            # tempB.language += pow(item[i], m) * points[j].lab

            j += 1

        # cent.append(Student(points[i].name, tempB.score / tempA.score, tempB.level_eng / tempA.level_eng, tempB.language / tempA.language))
        cent.append(Student(points[i].name, tempB.score / tempA.score, tempB.level_eng / tempA.level_eng, 0))
    return cent

def evklidDistance(a,b):
    # return math.sqrt((a.score - b.score)*(a.score - b.score)
    #                  +(a.level_eng - b.level_eng)*(a.level_eng - b.level_eng)+(a.lab - b.lab)*(a.lab - b.lab))
    return math.sqrt((a.score - b.score)*(a.score - b.score)
                     +(a.level_eng - b.level_eng)*(a.level_eng - b.level_eng)+0)

def prepareU(x, m):
    return pow(1/x, 2/(m-1))

def calculateFunction(arr, centers, matrix):
    s = 0
    for i in range(len(matrix)):
        for j in range(len(centers)):
            s += matrix[i][j] * evklidDistance(centers[j], arr[i])
    return s