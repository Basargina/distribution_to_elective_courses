import random
import pandas as pd
from courses.student import Student


def generateCenters(helper, a):
    points = []
    for i in range(helper.cluster_num()):
        student = Student(("группа № %s" % (a + 1)), random.randint(150, 300), random.randint(50, 100), 0)
        a += 1
        points.append(student)
    return points


def generateValues(filename='test_lists.csv'):
    # input_filename = input("Введите название файла с данными о студентах: ") + ".csv"

    # загрузка данных
    data = pd.read_csv(filename, encoding='windows-1251', sep=";", header=0)

    array = []
    for i in range(len(data)):
        student = Student(data.iloc[i]['fullname'], data.iloc[i]['score'], data.iloc[i]['level_eng'],
                          data.iloc[i]['language'])
        array.append(student)

    return array
