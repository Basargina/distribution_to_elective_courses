import courses.helpers as helpers


def course_for_student(points, matrix, centers):
    result = {'student': [], 'best_course': [], 'possible': []}
    for i in range(len(points)):
        result.get("student").append(points[i])
        max_cource = matrix[i].index(max(matrix[i]))
        result.get("best_course").append(centers[max_cource].name)
        possible = []
        for item in matrix[i]:
            if item > 0.1 and matrix[i].index(item) != max_cource:
                possible.append(centers[matrix[i].index(item)].name)
        result.get("possible").append(possible)
    return result


def make_groups(student_course, centers):
    result = {}
    for i in range(len(centers)):
        result.update({centers[i].name: []})
    for i in range(len(student_course.get('student'))):
        result[student_course['best_course'][i]].append(student_course['student'][i])
    return result


def max_student(result):
    max = 0
    max_2 = 0
    max_name = ''
    for item in result.keys():
        len_item = len(result[item])
        if len_item > max:
            max_2 = max
            max = len_item
            max_name = item
        if max_2 < len_item != max:
            max_2 = len_item
    return max_name, max, max_2


def make_normalized_groups(student_course, centers):
    result = make_groups(student_course, centers)
    flag = True
    while (flag):
        flag = False
        group_name, max, max_2 = max_student(result)
        modify_group = []
        k = 0
        for item in result[group_name]:
            courses = student_course['possible'][student_course['student'].index(item)]
            if len(courses) > 0 and max - k < max_2:
                flag = True
                result.get(courses[0]).append(item)
                student_course['possible'][student_course['student'].index(item)].pop(0)
                k += 1
            else:
                modify_group.append(item)
        result[group_name] = modify_group
    return result


def modernization_groups(groups):
    for g in groups.keys():
        groups[g] = {"students": groups[g], "scores": 0, "normal": False}
    return groups


def distribution_manual(groups):
    # groups = make_groups(student_course, centers)
    max = 0
    group_name = ''
    sum_students = 0
    for g in groups.keys():
        students_in_group = groups[g]["students"]
        count = len(students_in_group)
        if count > max:
            max = count
            group_name = g
        scores = (sum(stud.score for stud in students_in_group) / count) + (
                sum(stud.level_eng for stud in students_in_group) / count)
        groups[g]["scores"] = scores
        sum_students += count

    avg_amount = int(sum_students / len(groups))
    delta = max - avg_amount
    print(avg_amount)
    min_distance = 400
    group_name_second = ''
    n = groups[group_name]["scores"]
    for g in groups.keys():
        difference = abs(n - groups[g]["scores"])
        if group_name != g and min_distance > difference and not groups[g]["normal"]:
            min_distance = difference
            group_name_second = g

    print("%s -> %s" % (group_name, group_name_second))
    groups[group_name]["students"].sort(key=lambda x: (x.score, x.level_eng), reverse=True)
    print("delta", delta)
    if n < groups[group_name_second]["scores"]:
        groups[group_name]["students"].reverse()
    for stud in groups[group_name]["students"][:delta]:
        groups[group_name_second]["students"].append(stud)
    groups[group_name]["students"] = groups[group_name]["students"][delta:]
    groups[group_name]["normal"] = True
    return groups
