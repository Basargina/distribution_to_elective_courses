import csv
import random

import pandas as pd

from sklearn.cluster import KMeans

from courses import generate_data


def print_data(data_groups):
    for key in data_groups:
        print("Group %s - %s students" % (key, len(data_groups[key])))
        print()
        for j in data_groups[key]:
            print(j)
        print()
        print("--------------")
        print()


def create_file(filename, output_data):
    with open(filename, "w", newline="") as file:
        columns = ["number", "fullname", "score", "language", "level_eng", "group"]
        writer = csv.DictWriter(file, fieldnames=columns)
        writer.writeheader()
        i = 1
        for key in output_data:
            for u in output_data[key]:
                user = {"number": i, "fullname": u[1], "score": u[2],
                        "language": u[3], "level_eng": u[4], "group": "11-70%s" % (key + 1)}
                i += 1
                writer.writerow(user)


def init_empty_list_of_group(a, b):
    list_groups = {}
    k = b - a
    for i in range(k):
        list_groups[a] = []
        a += 1
    return list_groups


def clustering(data_for_cluster, data, list_groups):
    # кластеризация, указываем количество групп, и передаем данные
    kmeans = KMeans(n_clusters=len(list_groups), random_state=1).fit(data_for_cluster)
    array = kmeans.labels_
    data = data.as_matrix()
    i = 0
    for a in array:
        list_groups[a].append(data[i])
        i += 1
    return list_groups


def generate_with_k_means(data, groups, data_for_cluster):
    # Выделим данные, подлежащие анализу, баллы, язык, уровень английского
    data_for_cluster = data_for_cluster.ix[:, 1:]
    groups = clustering(data_for_cluster, data, groups)
    return groups


def print_students(array):
    for a in array:
        print(a)


def distribution_lang(array):
    dict_language = {}
    for stud in array:
        if dict_language.get(stud.language) is None:
            dict_language[stud.language] = []
        dict_language[stud.language].append(stud)
    return dict_language


def distribution_random(array, groups):
    b = int(len(array) / len(groups))
    for g in groups:
        for x in range(b):
            s = random.choice(array)
            groups[g].append(s)
            array.remove(s)
    if len(array) != 0:
        for g in groups:
            if len(array) == 0:
                break
            s = random.choice(array)
            groups[g].append(s)
            array.remove(s)
    return groups


def distribution_score_or_eng(array, groups, name_param):
    for i in range(len(array)):
        for j in range(len(array)):
            if array[i].get(name_param) > array[j].get(name_param):
                m = array[j]
                array[j] = array[i]
                array[i] = m
    print_students(array)
    a = 0
    b = int(len(array) / len(groups))
    remainder = len(array) - b * len(groups)
    print(remainder)
    for g in groups.keys():
        i = 0
        if remainder > 0:
            i = 1
            remainder -= 1
        groups[g] = array[a:a + b + i]
        a += b + i
    return groups


if __name__ == "__main__":
    input_filename = input("Введите название файла с данными о студентах: ") + ".csv"

    # загрузка данных
    data = pd.read_csv(input_filename, encoding='windows-1251', sep=";", header=0)

    # ввод количества групп
    k = int(input("Введите количество групп: "))

    filename = input("Введите название файла, в который будут записаны списки групп: ") + ".csv"

    # удаление ненужных столбцов
    data_for_cluster = data.drop(["number"], axis=1)
    # избавление от строковых параметров,
    # привести язык программирования к уровню Java - 100, C# - 200
    data_for_cluster.loc[data_for_cluster.language == 'Java', 'language'] = 50
    data_for_cluster.loc[data_for_cluster.language == 'C#', 'language'] = 100
    a = 0
    all_groups = generate_with_k_means(data, init_empty_list_of_group(a, k), data_for_cluster)
    array = generate_data.generateValues(input_filename)

    name_param = "level_eng"
    # groups = distribution_score(array, init_empty_list_of_group(k))


    # 1_random
    # a = 0
    # groups = distribution_random(array, init_empty_list_of_group(a, k))
    # print_data(groups)

    # 2_score

    # a = 0
    # groups = distribution_score_or_eng(array, init_empty_list_of_group(a, k), name_param)
    # print_data(groups)

    # 3_score_language


    # all_groups = {}
    # dict_language = distribution_lang(array)
    # i = 0
    # for key in dict_language:
    #     current_group_n = round(k * len(dict_language[key]) / len(array))
    #     all_groups.update(
    #         distribution_score_or_eng(dict_language[key], init_empty_list_of_group(i, current_group_n + i), name_param))
    #     i += current_group_n

    # 4_language
    # all_groups = {}
    # dict_language = distribution_lang(array)
    # i = 0
    # for key in dict_language:
    #     current_group_n = round(k * len(dict_language[key]) / len(array))
    #     all_groups.update(
    #         distribution_random(dict_language[key], init_empty_list_of_group(i, current_group_n + i)))
    #     i += current_group_n


    # SORT
    # for g in all_groups:
    #     all_groups[g].sort(key=lambda x: x.name, reverse=False)

    for g in all_groups:
        all_groups[g].sort(key=lambda x: x[1], reverse=False)

    print_data(all_groups)

    create_file(filename, all_groups)
