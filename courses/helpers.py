class Helper:

    def __init__(self, n):
        self.num = n

    def eps(self):
        return 0.1

    def max_cycles(self):
        return 10

    def points_count(self):
        return 200

    def cluster_num(self):
        return self.num

    def fuzz(self):
        return 1.5