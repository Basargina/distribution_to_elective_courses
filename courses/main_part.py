import courses.clustering as clustering
import courses.helpers as helpers
import courses.generate_data as generate_data
import courses.generate_group as generate_group


def get_result_groups(n, students, a):
    helper = helpers.Helper(n)
    centers = generate_data.generateCenters(helper, a)

    # запуск кластеризациии
    matrix = clustering.clustering(students, helper, centers)

    student_course = generate_group.course_for_student(students, matrix, centers)

    # print("Student - group")
    # for i in range(len(students)):
    #     print("%s - %s - %s" % (
    #         student_course["student"][i], student_course["best_course"][i], student_course["possible"][i]))

    # course_group = generate_group.make_groups(student_course, centers)
    # print(course_group)
    # for item in course_group.keys():
    #     print("%s - %s человек" % (item, len(course_group[item])))
    #
    norm_course_group = generate_group.make_normalized_groups(student_course, centers)
    groups = generate_group.modernization_groups(norm_course_group)
    for i in range(len(groups) - 1):
        print("i", i)
        groups = generate_group.distribution_manual(groups)

    for g in groups:
        groups[g]["students"].sort(key=lambda x: x.name, reverse=False)
    for item in groups.keys():
        print("%s - %s человек" % (item, len(groups[item]["students"])))
        for student in groups[item]["students"]:
            print(student)


if __name__ == "__main__":
    array_stud = generate_data.generateValues()
    num = len(array_stud)
    n_group = int(input("Введите количество групп > "))

    dict_language = {}
    for stud in array_stud:
        if dict_language.get(stud.language) is None:
            dict_language[stud.language] = []
        dict_language[stud.language].append(stud)

    for key in dict_language:
        print(key)
        print(len(dict_language[key]))
        for stud in dict_language[key]:
            print(stud)
    i = 0
    for key in dict_language:
        current_group_n = round(n_group * len(dict_language[key]) / len(array_stud))
        print(current_group_n)
        get_result_groups(current_group_n, dict_language[key], i)
        i += current_group_n
