class Student:

    def __init__(self, name, average_point, courses, z):
        self.name = name
        self.score = average_point
        self.level_eng = courses
        self.language = z

    def __str__(self):
        return "%s - score - %s eng - %s language - %s " % (self.name, self.score, self.level_eng, self.language)

    def get(self, name_param):
        if name_param == "level_eng":
            return self.level_eng
        else:
            return self.score